﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Timers;

namespace Documents
{
    public class DocumentsReceiver : IDisposable
    {
        private FileSystemWatcher _watcher;
        private Timer _timer;
        private readonly List<string> _documentList;

        public event Action DocumentsReady;
        public event Action TimedOut;

        public DocumentsReceiver(IEnumerable<string> documentList)
        {
            _documentList = documentList.ToList();
        }

        public void Start(string targetDirectory, int waitingInterval)
        {
            _watcher = new FileSystemWatcher(targetDirectory);
            _watcher.Changed += OnChanged;
            _watcher.EnableRaisingEvents = true;

            _timer = new Timer(waitingInterval);
            _timer.Elapsed += IntervalElapsed;
            _timer.Enabled = true;
        }

        private void Stop()
        {
            _watcher.Changed -= OnChanged;
            _timer.Elapsed -= IntervalElapsed;
        }

        private void OnChanged(object sender, FileSystemEventArgs e)
        {
            var files = Directory.EnumerateFiles(_watcher.Path).ToList();

            if (files.Count > 2)
            {
                foreach (var document in _documentList)
                {
                    if (!files.Contains(Path.Combine(_watcher.Path, document)))
                    {
                        return;
                    }
                }

                Stop();

                DocumentsReady?.Invoke();
            }
        }

        private void IntervalElapsed(object sender, ElapsedEventArgs e)
        {
            Stop();

            TimedOut?.Invoke();
        }

        public void Dispose()
        {
            _watcher.Dispose();

            _timer.Dispose();
        }
    }
}
