﻿using Documents;
using System;
using System.Collections.Generic;
using System.IO;

namespace AcceptanceOfDocuments
{
    class Program
    {
        static private bool stop;

        static void Main(string[] args)
        {
            const string targetDirectory = "Documents";
            const int waitingInterval = 50000;

            var files = new List<string>()
            {
                "Паспорт.jpg",
                "Заявление.TXT",
                "Фото.jpg"
            };

            if (!Directory.Exists(targetDirectory))
            {
                Directory.CreateDirectory(targetDirectory);
            }

            using var dr = new DocumentsReceiver(files);

            dr.DocumentsReady += DocumentsReady;
            dr.TimedOut += TimedOut;

            dr.Start(targetDirectory, waitingInterval);

            Console.Write("Uploading documents");

            while (!stop) ;

            Console.ReadKey();
        }

        static void DocumentsReady()
        {
            Console.Clear();
            Console.WriteLine("Documents uploaded.");

            stop = true;
        }

        static void TimedOut()
        {
            Console.Clear();
            Console.WriteLine("Timed out! Download stopped.");

            stop = true;
        }
    }
}
